import copy
import os
import random
import time
from datetime import date
from mpl_toolkits import mplot3d
from pandas import DataFrame
from scipy.integrate import solve_ivp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import plotly.graph_objects as go


def enzyme_kinetics(step_count):
    # michalis menten constants KM = (kr + kcat)/kf are normally in 10^-7 to 10^-1 mol/L
    # base of constants: 0.001, 0.001, 0.001
    rate_constants = [0.001, 0.001, 0.001]  # [kf, kr, kcat]
    # rate_constants = [0.01, 0.01, 0.01]
    enzyme_parameters = []

    for enzyme in range(2 * step_count):
        enzyme_parameters.append(copy.deepcopy(rate_constants))

    return enzyme_parameters


def initial_state(starting_operational_params):
    step_count = int(starting_operational_params[0])
    substrate_start = float(starting_operational_params[2])  # substrate starting/initial concentration
    initial_state_vector = []  # will have form: (enzyme, substrate, enzyme-substrate-complexes, products)

    if step_count == 1:
        # enzyme starting concentrations
        enzyme_1 = float(starting_operational_params[5])
        enzyme_2 = float(starting_operational_params[6])

        # add heterozygous genotype
        initial_state_vector.append([enzyme_1, enzyme_2, substrate_start, 0, 0, 0, 0])
        # add homozygous genotype for enzyme_1
        initial_state_vector.append([2 * enzyme_1, 0, substrate_start, 0, 0, 0, 0])
        # add homozygous genotype for enzyme_2
        initial_state_vector.append([0, 2 * enzyme_2, substrate_start, 0, 0, 0, 0])
    elif step_count == 2:
        enzyme_1 = float(starting_operational_params[5])
        enzyme_2 = float(starting_operational_params[6])
        enzyme_3 = float(starting_operational_params[7])
        enzyme_4 = float(starting_operational_params[8])
        for i in range(3):
            enzyme_1_proxy = 0
            enzyme_2_proxy = 0

            if i == 0:
                enzyme_1_proxy = enzyme_1
                enzyme_2_proxy = enzyme_2
            elif i == 1:
                enzyme_1_proxy = 2 * enzyme_1
                enzyme_2_proxy = 0
            elif i == 2:
                enzyme_1_proxy = 0
                enzyme_2_proxy = 2 * enzyme_2
            for j in range(3):
                if j == 0:
                    initial_state_vector.append(
                        [enzyme_1_proxy, enzyme_2_proxy, enzyme_3, enzyme_4,
                         substrate_start, 0.0, 0, 0, 0, 0, 0, 0, 0, 0])
                elif j == 1:
                    initial_state_vector.append(
                        [enzyme_1_proxy, enzyme_2_proxy, 2 * enzyme_3, 0 * enzyme_4,
                         substrate_start, 0.0, 0, 0, 0, 0, 0, 0, 0, 0])
                elif j == 2:
                    initial_state_vector.append(
                        [enzyme_1_proxy, enzyme_2_proxy, 0 * enzyme_3, 2 * enzyme_4,
                         substrate_start, 0.0, 0, 0, 0, 0, 0, 0, 0, 0])

    return initial_state_vector


def ODE_system_oneStep(t, z, parameters):
    next_state_vector = [0] * len(z)
    # perturbation = parameters[-1][1]
    # standard_deviation = 0.2

    """if perturbation:
        kf1 = parameters[0][0] * abs(np.random.normal(loc=1, scale=standard_deviation))
        kr1 = parameters[0][1] * abs(np.random.normal(loc=1, scale=standard_deviation))
        kcat1 = parameters[0][2] * abs(np.random.normal(loc=1, scale=standard_deviation))
        kf2 = parameters[1][0] * abs(np.random.normal(loc=1, scale=standard_deviation))
        kr2 = parameters[1][1] * abs(np.random.normal(loc=1, scale=standard_deviation))
        kcat2 = parameters[1][2] * abs(np.random.normal(loc=1, scale=standard_deviation))

        substrate_flux = parameters[-1][0] * abs(np.random.normal(loc=1, scale=standard_deviation))
        # substrate_flux += random.uniform(-0.0001 * substrate_flux, 0.0001 * substrate_flux)
    else:"""
    kf1 = parameters[0][0]
    kr1 = parameters[0][1]
    kcat1 = parameters[0][2]
    kf2 = parameters[1][0]
    kr2 = parameters[1][1]
    kcat2 = parameters[1][2]

    substrate_flux = parameters[-1][0]

    # name variables
    enzyme_1 = z[0]
    enzyme_2 = z[1]
    substrate = z[2]
    complex_1 = z[3]
    complex_2 = z[4]

    # calculate next state (change) of ...
    # ...enzyme_1 and enzyme_2 concentration
    next_state_vector[0] = -kf1 * enzyme_1 * substrate + kr1 * complex_1 + kcat1 * complex_1
    next_state_vector[1] = -kf2 * enzyme_2 * substrate + kr2 * complex_2 + kcat2 * complex_2
    # ...substrate concentration
    next_state_vector[2] = (-kf1 * enzyme_1 * substrate + kr1 * complex_1) + \
                           (-kf2 * enzyme_2 * substrate + kr2 * complex_2) + substrate_flux
    # ...complex_1 and complex_2 concentration
    next_state_vector[3] = kf1 * enzyme_1 * substrate - kr1 * complex_1 - kcat1 * complex_1
    next_state_vector[4] = kf2 * enzyme_2 * substrate - kr2 * complex_2 - kcat2 * complex_2
    # ...product_1 and product_2 concentration
    next_state_vector[5] = kcat1 * complex_1
    next_state_vector[6] = kcat2 * complex_2

    return next_state_vector


def ODE_system_twoStep(t, z, parameters):
    next_state_vector = [0] * len(z)
    perturbation = parameters[-1][1]

    # name variables
    # enzymes
    enzyme_1 = z[0]
    enzyme_2 = z[1]
    enzyme_3 = z[2]
    enzyme_4 = z[3]
    # substrates
    substrate_1 = z[4]
    substrate_2 = z[5]
    # enzyme-substrate-complexes
    complex_1 = z[6]
    complex_2 = z[7]
    complex_3 = z[8]
    complex_4 = z[9]

    if perturbation:
        # name parameters
        # enzyme 1
        kf1 = parameters[0][0] + random.uniform(-0.0005, 0.0005)
        kr1 = parameters[0][1] + random.uniform(-0.005, 0.005)
        kcat1 = parameters[0][2] + random.uniform(-0.005, 0.005)
        # enzyme 1'
        kf2 = parameters[1][0] + random.uniform(-0.0005, 0.0005)
        kr2 = parameters[1][1] + random.uniform(-0.005, 0.005)
        kcat2 = parameters[1][2] + random.uniform(-0.005, 0.005)
        # enzyme 2
        kf3 = parameters[2][0] + random.uniform(-0.0005, 0.0005)
        kr3 = parameters[2][1] + random.uniform(-0.005, 0.005)
        kcat3 = parameters[2][2] + random.uniform(-0.005, 0.005)
        # enzyme 2'
        kf4 = parameters[3][0] + random.uniform(-0.0005, 0.0005)
        kr4 = parameters[3][1] + random.uniform(-0.005, 0.005)
        kcat4 = parameters[3][2] + random.uniform(-0.005, 0.005)

        substrate_flux_firstStep = parameters[len(parameters) - 1][0]
        substrate_flux_firstStep += random.uniform(-0.01 * substrate_flux_firstStep, 0.01 * substrate_flux_firstStep)
        substrate_flux_secondStep = kcat1 * complex_1 + kcat2 * complex_2
    else:
        # name parameters
        # enzyme 1
        kf1 = parameters[0][0]
        kr1 = parameters[0][1]
        kcat1 = parameters[0][2]
        # enzyme 1'
        kf2 = parameters[1][0]
        kr2 = parameters[1][1]
        kcat2 = parameters[1][2]
        # enzyme 2
        kf3 = parameters[2][0]
        kr3 = parameters[2][1]
        kcat3 = parameters[2][2]
        # enzyme 2'
        kf4 = parameters[3][0]
        kr4 = parameters[3][1]
        kcat4 = parameters[3][2]

        substrate_flux_firstStep = parameters[-1][0]
        substrate_flux_secondStep = kcat1 * complex_1 + kcat2 * complex_2

    # calculate next state of ...
    # ... enzyme concentrations
    next_state_vector[0] = -kf1 * enzyme_1 * substrate_1 + kr1 * complex_1 + kcat1 * complex_1
    next_state_vector[1] = -kf2 * enzyme_2 * substrate_1 + kr2 * complex_2 + kcat2 * complex_2
    next_state_vector[2] = -kf3 * enzyme_3 * substrate_2 + kr3 * complex_3 + kcat3 * complex_3
    next_state_vector[3] = -kf4 * enzyme_4 * substrate_2 + kr4 * complex_4 + kcat4 * complex_4
    # ... substrate concentrations
    next_state_vector[4] = (-kf1 * enzyme_1 * substrate_1 + kr1 * complex_1) + \
                           (-kf2 * enzyme_2 * substrate_1 + kr2 * complex_2) + substrate_flux_firstStep
    next_state_vector[5] = (-kf3 * enzyme_3 * substrate_2 + kr3 * complex_3) + \
                           (-kf4 * enzyme_4 * substrate_2 + kr4 * complex_4) + substrate_flux_secondStep
    # ... enzyme-substrate-complex concentrations
    next_state_vector[6] = kf1 * enzyme_1 * substrate_1 - kr1 * complex_1 - kcat1 * complex_1
    next_state_vector[7] = kf2 * enzyme_2 * substrate_1 - kr2 * complex_2 - kcat2 * complex_2
    next_state_vector[8] = kf3 * enzyme_3 * substrate_2 - kr3 * complex_3 - kcat3 * complex_3
    next_state_vector[9] = kf4 * enzyme_4 * substrate_2 - kr4 * complex_4 - kcat4 * complex_4
    # ... product concentrations
    next_state_vector[10] = kcat1 * complex_1
    next_state_vector[11] = kcat2 * complex_2
    next_state_vector[12] = kcat3 * complex_3
    next_state_vector[13] = kcat4 * complex_4

    return next_state_vector


def make_graph_oneStep(ode_solution, t, genotype, axEnzyme, axProduct):
    # workdir = os.getcwd()
    # title_append = ""
    title_1 = ""
    title_2 = ""

    if genotype == 0:
        # title_append = "heterozygous"
        title_1 = "(a)"
        title_2 = "(b)"
    elif genotype == 1:
        # title_append = "homozygous for e1"
        title_1 = "(c)"
        title_2 = "(d)"
    elif genotype == 2:
        # title_append = "homozygous for e1'"
        title_1 = "(e)"
        title_2 = "(f)"

    # path = workdir + "\\concentrations_" + title_append + ".pdf"

    # with PdfPages(path) as export_pdf:

    """plt.plot(t, ode_solution[0].T, label="e1")
    plt.plot(t, ode_solution[1].T, label="e1'")
    plt.plot(t, ode_solution[3].T, label="c1")
    plt.plot(t, ode_solution[4].T, label="c1'")

    plt.title(title_append)
    plt.xlabel("time")
    plt.ylabel("concentration")
    plt.legend(shadow=True)
    plt.grid()
    #export_pdf.savefig()
    #plt.close()

    plt.plot(t, ode_solution[5].T, label="p1")
    plt.plot(t, ode_solution[6].T, label="p1'")
    plt.plot(t, ode_solution[2].T, label="s")
    plt.title(title_append)
    plt.xlabel("time")
    plt.ylabel("concentration")
    plt.grid()
    plt.legend(shadow=True)
    #export_pdf.savefig()
    #plt.close()"""
    # plt.rc('font', size=22)

    axEnzyme.plot(t, ode_solution[0].T, label="e1")
    axEnzyme.plot(t, ode_solution[1].T, label="e1'")
    axEnzyme.plot(t, ode_solution[3].T, label="c1")
    axEnzyme.plot(t, ode_solution[4].T, label="c1'")
    if genotype == 2:
        axEnzyme.set_xlabel("time")
    axEnzyme.set_ylabel("concentration")
    axEnzyme.set_title(title_1, loc="left")
    axEnzyme.legend(shadow=True)
    axEnzyme.grid()

    axProduct.plot(t, ode_solution[5].T, label="p1")
    axProduct.plot(t, ode_solution[6].T, label="p1'")
    axProduct.plot(t, ode_solution[2].T, label="s")
    if genotype == 2:
        axProduct.set_xlabel("time")
    # axProduct.set_ylabel("concentration")
    axProduct.set_title(title_2, loc="left")
    axProduct.legend(shadow=True)
    axProduct.grid()


def make_graph_twoStep(ode_solution, t, genotype):
    workdir = os.getcwd()
    today = date.today()
    path = workdir + "\\product_generation_genotype-" + str(genotype) + ".pdf"

    with PdfPages(path) as export_pdf:
        # plot first step
        plt.plot(t, ode_solution[10].T, label="p1")
        plt.plot(t, ode_solution[11].T, label="p1'")

        if genotype < 3:
            if genotype == 0:
                plt.title("step one heterozygous; step two heterozygous\nPRODUCTS")
            if genotype == 1:
                plt.title("step one heterozygous; step two homozygous e2\nPRODUCTS")
            if genotype == 2:
                plt.title("step one heterozygous; step two homozygous e2'\nPRODUCTS")
        elif genotype < 6:
            if genotype == 3:
                plt.title("step one homozygous e1; step two heterozygous\nPRODUCTS")
            if genotype == 4:
                plt.title("step one homozygous e1; step two homozygous e2\nPRODUCTS")
            if genotype == 5:
                plt.title("step one homozygous e1; step two homozygous e2'\nPRODUCTS")
        else:
            if genotype == 6:
                plt.title("step one homozygous e1'; step two heterozygous\nPRODUCTS")
            if genotype == 7:
                plt.title("step one homozygous e1'; step two homozygous e2\nPRODUCTS")
            if genotype == 8:
                plt.title("step one homozygous e1'; step two homozygous e2'\nPRODUCTS")

        plt.legend(shadow=True)
        plt.grid()
        export_pdf.savefig()
        plt.close()

        # plot enzyme concentrations separately
        plt.plot(t, ode_solution[0].T, label="e1")
        plt.plot(t, ode_solution[1].T, label="e1'")
        plt.plot(t, ode_solution[6].T, label="c1")
        plt.plot(t, ode_solution[7].T, label="c1'")

        if genotype < 3:
            if genotype == 0:
                plt.title("step one heterozygous; step two heterozygous\nPRODUCTS")
            if genotype == 1:
                plt.title("step one heterozygous; step two homozygous e2\nPRODUCTS")
            if genotype == 2:
                plt.title("step one heterozygous; step two homozygous e2'\nPRODUCTS")
        elif genotype < 6:
            if genotype == 3:
                plt.title("step one homozygous e1; step two heterozygous\nPRODUCTS")
            if genotype == 4:
                plt.title("step one homozygous e1; step two homozygous e2\nPRODUCTS")
            if genotype == 5:
                plt.title("step one homozygous e1; step two homozygous e2'\nPRODUCTS")
        else:
            if genotype == 6:
                plt.title("step one homozygous e1'; step two heterozygous\nPRODUCTS")
            if genotype == 7:
                plt.title("step one homozygous e1'; step two homozygous e2\nPRODUCTS")
            if genotype == 8:
                plt.title("step one homozygous e1'; step two homozygous e2'\nPRODUCTS")

        plt.legend(shadow=True)
        plt.grid()
        export_pdf.savefig()
        plt.close()

        # plot second step
        plt.plot(t, ode_solution[12].T, label="p2")
        plt.plot(t, ode_solution[13].T, label="p2'")

        if genotype == 0:
            plt.title("step two heterozygous; step one heterozygous\nENZYMES & COMPLEXES")
        if genotype == 1:
            plt.title("step two homozygous e2; step one heterozygous\nENZYMES & COMPLEXES")
        if genotype == 2:
            plt.title("step two homozygous e2'; step one heterozygous\nENZYMES & COMPLEXES")
        if genotype == 3:
            plt.title("step two heterozygous; step one homozygous e1\nENZYMES & COMPLEXES")
        if genotype == 4:
            plt.title("step two homozygous e2; step one homozygous e1\nENZYMES & COMPLEXES")
        if genotype == 5:
            plt.title("step two homozygous e2'; step one homozygous e1\nENZYMES & COMPLEXES")
        if genotype == 6:
            plt.title("step two heterozygous; step one homozygous e1'\nENZYMES & COMPLEXES")
        if genotype == 7:
            plt.title("step two homozygous e2; step one homozygous e1'\nENZYMES & COMPLEXES")
        if genotype == 8:
            plt.title("step two homozygous e2'; step one homozygous e1'\nENZYMES & COMPLEXES")

        plt.legend(shadow=True)
        plt.grid()
        export_pdf.savefig()
        plt.close()

        # plot enzyme concentrations separately
        plt.plot(t, ode_solution[2].T, label="e2")
        plt.plot(t, ode_solution[3].T, label="e2'")
        plt.plot(t, ode_solution[8].T, label="c2")
        plt.plot(t, ode_solution[9].T, label="c2'")

        if genotype == 0:
            plt.title("step two heterozygous; step one heterozygous\nENZYMES & COMPLEXES")
        if genotype == 1:
            plt.title("step two homozygous e2; step one heterozygous\nENZYMES & COMPLEXES")
        if genotype == 2:
            plt.title("step two homozygous e2'; step one heterozygous\nENZYMES & COMPLEXES")
        if genotype == 3:
            plt.title("step two heterozygous; step one homozygous e1\nENZYMES & COMPLEXES")
        if genotype == 4:
            plt.title("step two homozygous e2; step one homozygous e1\nENZYMES & COMPLEXES")
        if genotype == 5:
            plt.title("step two homozygous e2'; step one homozygous e1\nENZYMES & COMPLEXES")
        if genotype == 6:
            plt.title("step two heterozygous; step one homozygous e1'\nENZYMES & COMPLEXES")
        if genotype == 7:
            plt.title("step two homozygous e2; step one homozygous e1'\nENZYMES & COMPLEXES")
        if genotype == 8:
            plt.title("step two homozygous e2'; step one homozygous e1'\nENZYMES & COMPLEXES")

        plt.legend(shadow=True)
        plt.grid()
        export_pdf.savefig()
        plt.close()


def calc_dominance_oneStep(products_1):
    dominance_result = []

    dominance_enzyme_1 = list(map(lambda het, hom1, hom2: ((round(het, 10) - round(hom2, 10)) /
                                                           (round(hom1, 10) - round(hom2, 10))) if round(hom1,
                                                                                                         10) != round(
        hom2, 10) else 0.5,
                                  products_1[0], products_1[1], products_1[2]))
    dominance_enzyme_2 = [1 - value for value in dominance_enzyme_1]

    dominance_result.append(dominance_enzyme_1)
    dominance_result.append(dominance_enzyme_2)

    return dominance_result


def calc_dominance_twoStep(products_1, products_2):
    dominance_result_twoStep = []
    dominance_result_1 = []
    # dominance_result_1_fitness = []  # dominance of first step related to products of second
    dominance_result_2 = []

    for i in range(3):
        # calculate dominance for step one
        dominance_enzyme_1 = list(map(lambda het1, hom1, hom2: ((round(het1, 10) - round(hom2, 10)) /
                                                                (round(hom1, 10) - round(hom2, 10))) if round(hom1,
                                                                                                              10) != round(
            hom2, 10) else 0.5,
                                      products_1[i], products_1[i + 3], products_1[i + 6]))
        dominance_enzyme_1_fitness = list(map(lambda het1, hom1, hom2: ((round(het1, 10) - round(hom2, 10)) /
                                                                        (round(hom1, 10) - round(hom2, 10))) if round(
            hom1, 10) != round(hom2, 10) else 0.5,
                                              products_2[i], products_2[i + 3], products_2[i + 6]))
        dominance_result_1.append([dominance_enzyme_1, dominance_enzyme_1_fitness])

        # calculate dominance for step two
        dominance_enzyme_3 = list(map(lambda het2, hom3, hom4: ((round(het2, 10) - round(hom4, 10)) /
                                                                (round(hom3, 10) - round(hom4, 10))) if round(hom3,
                                                                                                              10) != round(
            hom4, 10) else 0.5,
                                      products_2[3 * i], products_2[3 * i + 1], products_2[3 * i + 2]))
        dominance_enzyme_4 = [1 - value for value in dominance_enzyme_3]
        dominance_result_2.append([dominance_enzyme_3, dominance_enzyme_4])

    dominance_result_twoStep.append(dominance_result_1)
    dominance_result_twoStep.append(dominance_result_2)

    return dominance_result_twoStep


def make_dominance_graph_oneStep(t, dominance_result, ax3):
    # note: maybe put workdir before function calls to reduce redundancy
    # workdir = os.getcwd()
    # path = workdir + "\\dominance_oneStep.pdf"

    # with PdfPages(path) as export_pdf:

    """plt.plot(t, dominance_result[0], label="d(e1)")
    plt.plot(t, dominance_result[1], label="d(e1')")
    plt.title("dominance values for one step")
    plt.xlabel("time")
    plt.ylabel("dominance")
    plt.legend(shadow=True)
    plt.grid()
    export_pdf.savefig()
    plt.close()"""
    ax3.plot(t, dominance_result[0], label=r"$d_A$", linewidth=7)
    ax3.set_title("(c)", loc="left")
    ax3.set_xlabel("time")
    ax3.set_ylabel("dominance")
    ax3.legend(shadow=True, prop={"size": 14})
    ax3.set_ylim(0, 1)
    ax3.grid()


def make_dominance_graph_twoStep(t, dominance_result_twoStep):
    workdir = os.getcwd()
    path = workdir + "\\dominance.pdf"

    with PdfPages(path) as export_pdf:

        for i in range(2):
            for j in range(3):
                dominance_forGenotype = dominance_result_twoStep[i][j]
                plot_title = ""

                if i == 0:
                    label_1 = "d(e1)"
                    label_2 = "d(e1')"
                    plot_title += "dominance step one / "
                else:
                    label_1 = "d(e2)"
                    label_2 = "d(e2')"
                    plot_title += "dominance step two / "

                if j == 0:
                    plot_title += "other step heterozygous"
                elif j == 1:
                    plot_title += "other step homozygous for first enzyme"
                else:
                    plot_title += "other step homozygous for second enzyme"

                plt.plot(t, dominance_forGenotype[0], label=label_1)
                plt.plot(t, dominance_forGenotype[1], label=label_2)
                plt.title(plot_title)
                plt.legend(shadow=True)
                plt.grid()
                export_pdf.savefig()
                plt.close()


def reverse_fitness(step_count, viability_perGenotype, perturbation_mode):
    relative_fitness_reversed = []
    standard_deviation = 0.2
    middlepoint = 1

    if perturbation_mode:
        if step_count == 1:
            random_scale_heterozygous = abs(np.random.normal(loc=middlepoint, scale=standard_deviation))
            random_scale_homozygous_1 = abs(np.random.normal(loc=middlepoint, scale=standard_deviation))
            random_scale_homozygous_2 = abs(np.random.normal(loc=middlepoint, scale=standard_deviation))
            # append heterozygous genotype
            # relative_fitness_reversed.append(viability_perGenotype[0] * random_scale_heterozygous)
            # switch homozygous genotypes fitness values
            # relative_fitness_reversed.append(viability_perGenotype[2] * random_scale_homozygous_1)
            # relative_fitness_reversed.append(viability_perGenotype[1] * random_scale_homozygous_2)
            het7525 = 194.47502217022853 # heterozygous AB from A=75, B=25
            hom17525 = 202.2355447527341 # homozygous AA from A=75, B=25
            hom27525 = 126.58323472236921 # homozygous BB A=75, B=25
            relative_fitness_reversed.append(het7525)
            relative_fitness_reversed.append(hom27525)
            relative_fitness_reversed.append(hom17525)

            print("perturbations:",
                  "\n middlepoint:\t\t\t\t", middlepoint,
                  "\n standard deviation:\t\t", standard_deviation,
                  "\n random scale heterozygous:\t", random_scale_heterozygous,
                  "\n random scale homozygous A:\t", random_scale_homozygous_1,
                  "\n random scale homozygous B:\t", random_scale_homozygous_2)
        else:
            # AaBb -> AaBb
            relative_fitness_reversed.append(viability_perGenotype[0] * abs(np.random.normal(loc=1, scale=standard_deviation)))
            # AaBB -> Aabb
            relative_fitness_reversed.append(viability_perGenotype[2] * abs(np.random.normal(loc=1, scale=standard_deviation)))
            # Aabb -> AaBB
            relative_fitness_reversed.append(viability_perGenotype[1] * abs(np.random.normal(loc=1, scale=standard_deviation)))
            # AABb -> aaBb
            relative_fitness_reversed.append(viability_perGenotype[6] * abs(np.random.normal(loc=1, scale=standard_deviation)))
            # AABB -> aabb
            relative_fitness_reversed.append(viability_perGenotype[8] * abs(np.random.normal(loc=1, scale=standard_deviation)))
            # AAbb -> aaBB
            relative_fitness_reversed.append(viability_perGenotype[7] * abs(np.random.normal(loc=1, scale=standard_deviation)))
            # aaBb -> AABb
            relative_fitness_reversed.append(viability_perGenotype[3] * abs(np.random.normal(loc=1, scale=standard_deviation)))
            # aaBB -> AAbb
            relative_fitness_reversed.append(viability_perGenotype[5] * abs(np.random.normal(loc=1, scale=standard_deviation)))
            # aabb -> AABB
            relative_fitness_reversed.append(viability_perGenotype[4] * abs(np.random.normal(loc=1, scale=standard_deviation)))
    else:
        if step_count == 1:
            # append heterozygous genotype
            relative_fitness_reversed.append(viability_perGenotype[0])
            # switch homozygous genotypes fitness values
            relative_fitness_reversed.append(viability_perGenotype[2])
            relative_fitness_reversed.append(viability_perGenotype[1])
        else:
            # AaBb -> AaBb
            relative_fitness_reversed.append(viability_perGenotype[0])
            # AaBB -> Aabb
            relative_fitness_reversed.append(viability_perGenotype[2])
            # Aabb -> AaBB
            relative_fitness_reversed.append(viability_perGenotype[1])
            # AABb -> aaBb
            relative_fitness_reversed.append(viability_perGenotype[6])
            # AABB -> aabb
            relative_fitness_reversed.append(viability_perGenotype[8])
            # AAbb -> aaBB
            relative_fitness_reversed.append(viability_perGenotype[7])
            # aaBb -> AABb
            relative_fitness_reversed.append(viability_perGenotype[3])
            # aaBB -> AAbb
            relative_fitness_reversed.append(viability_perGenotype[5])
            # aabb -> AABB
            relative_fitness_reversed.append(viability_perGenotype[4])

    return relative_fitness_reversed


def allele_frequency_calc(step_count, discrete_generations, products, perturbation_mode):
    product_forFitness = products[-1]  # aka product of "last" step
    viability_perGenotype = calc_viability(product_forFitness)
    init_frequency = 0.5  # initial allele frequency
    allele_frequencies_result = []
    proportion_genotype_change = []
    standard_deviation = 0.05  # for binomial distribution; to draw random values for perturbations from

    for i in range(3 ** step_count):
        proportion_genotype_change.append([])

    # genotypes possible for one/two step(s)
    # the first enzyme per step is referenced by "0" its alternative form by "1"
    if step_count == 1:
        genotype_vector = [["01"], ["00"], ["11"]]
    else:
        genotype_vector = [["01", "01"], ["01", "00"], ["01", "11"],
                           ["00", "01"], ["00", "00"], ["00", "11"],
                           ["11", "01"], ["11", "00"], ["11", "11"]]

    # include initial frequency value into result
    for i in range(step_count):
        # stores information of allele frequency of first alleles of each step
        allele_frequencies_result.append([init_frequency])

    # calculate allele frequencies per generation
    relative_fitness = viability_perGenotype
    relative_fitness_reversed = reverse_fitness(step_count, viability_perGenotype, perturbation_mode)
    season_duration = 20  # generation count spanning one (environmental) season
    season_counter = 0

    for generation in range(discrete_generations):
        # probability for the occurrence of a genotype in the population
        proportion_genotype = proportion_genotype_inPopulation(step_count, genotype_vector, allele_frequencies_result)
        proportion_genotype_change[0].append(proportion_genotype[0])
        proportion_genotype_change[1].append(proportion_genotype[1])
        proportion_genotype_change[2].append(proportion_genotype[2])
        if step_count == 2:
            proportion_genotype_change[3].append(proportion_genotype[3])
            proportion_genotype_change[4].append(proportion_genotype[4])
            proportion_genotype_change[5].append(proportion_genotype[5])
            proportion_genotype_change[6].append(proportion_genotype[6])
            proportion_genotype_change[7].append(proportion_genotype[7])
            proportion_genotype_change[8].append(proportion_genotype[8])

        # construct denominator
        denominator = frequency_calc_denominator(step_count, proportion_genotype, relative_fitness)

        # construct numerator
        if step_count == 1:
            numerator_1 = frequency_calc_numerator_firstStep(step_count, proportion_genotype, relative_fitness)
            allele_frequency_1 = numerator_1 / denominator
            allele_frequencies_result[0].append(allele_frequency_1)
        else:
            numerator_1 = frequency_calc_numerator_firstStep(step_count, proportion_genotype, relative_fitness)
            numerator_2 = frequency_calc_numerator_secondStep(proportion_genotype, relative_fitness)

            allele_frequency_1 = numerator_1 / denominator
            allele_frequency_2 = numerator_2 / denominator

            allele_frequencies_result[0].append(allele_frequency_1)
            allele_frequencies_result[1].append(allele_frequency_2)

        # apply symmetrical fitness reversal
        if (generation + 1) % season_duration == 0:
            if season_counter == 0:
                relative_fitness = relative_fitness_reversed
                season_counter = 1
            else:
                relative_fitness = viability_perGenotype
                season_counter = 0

    if step_count > 1:
        make_graph_proportion_genotype(proportion_genotype_change)

    return allele_frequencies_result


def calc_viability(product_forFitness):
    genotype_count = len(product_forFitness)
    viability_perGenotype = []

    for i in range(genotype_count):
        # viability equals product concentration of last time point
        viability_perGenotype.append(product_forFitness[i][-1])

    return viability_perGenotype


def proportion_genotype_inPopulation(step_count, genotype_vector, allele_frequencies):
    # calculate probabilities for genotype occurrence in population
    proportion_genotype = []

    for genotype in genotype_vector:
        probability = 1  # to be modified during calculation
        calculation_doc = ""
        for i in range(step_count):
            if genotype[i] == "01":
                probability *= (2 * allele_frequencies[i][-1]) * (1 - allele_frequencies[i][-1])
                calculation_doc += "2p" + str(i + 1) + " * (1 - p" + str(i + 1) + ")"
            elif genotype[i] == "00":
                probability *= allele_frequencies[i][-1] ** 2
                calculation_doc += "p" + str(i + 1) + "^2"
            else:
                probability *= (1 - allele_frequencies[i][-1]) ** 2
                calculation_doc += "(1 - p" + str(i + 1) + ")^2"
            if step_count > 1 and i == 0:
                calculation_doc += " * "
        proportion_genotype.append(probability)

    return proportion_genotype


def frequency_calc_denominator(step_count, proportion_genotype, viability_perGenotype):
    denominator = 0
    test_denominator = "denominator: "

    for i in range(3 ** step_count):
        denominator += proportion_genotype[i] * viability_perGenotype[i]
        test_denominator += "x" + str(i) + "w" + str(i)
        if i < (3 ** step_count - 1):
            test_denominator += " + "

    return denominator


def frequency_calc_numerator_firstStep(step_count, proportion_genotype, viability_perGenotype):
    numerator = 0
    test_numerator = "numerator_1: "

    if step_count == 1:
        numerator += (0.5 * proportion_genotype[0] * viability_perGenotype[0]) + \
                     (proportion_genotype[1] * viability_perGenotype[1])
    else:
        # all genotypes where allele 1 is present
        for i in range(6):
            factor = proportion_genotype[i] * viability_perGenotype[i]
            if i < 3:
                numerator += 0.5 * factor
                test_numerator += "0.5 * x" + str(i) + "w" + str(i)
            else:
                numerator += factor
                test_numerator += "x" + str(i) + "w" + str(i)
            if i < 5:
                test_numerator += " + "

    return numerator


def frequency_calc_numerator_secondStep(proportion_genotype, viability_perGenotype):
    numerator = 0
    # test_numerator = "numerator_2: "

    for i in range(9):
        if i % 3 == 0:
            factor = proportion_genotype[i] * viability_perGenotype[i]
            numerator += 0.5 * factor
            # test_numerator += "0.5 * x" + str(i) + "w" + str(i)

            factor = proportion_genotype[i + 1] * viability_perGenotype[i + 1]
            numerator += factor
            """test_numerator += " + x" + str(i + 1) + "w" + str(i + 1)
            if i < 6:
                test_numerator += " + """

    return numerator


def make_graph_alleleFrequency(step_count, allele_frequency_result, ax4):
    """workdir = os.getcwd()
    path = workdir + "\\allele_frequency.pdf"""

    if step_count == 1:
        """plt.plot(allele_frequency_result[0], label="allele1")
        plt.title("one step; allele frequency of first allele")
        plt.ylabel("allele frequency")
        plt.xlabel("generation")
        plt.legend(shadow=True)
        plt.grid()
        #export_pdf.savefig()
        #plt.close()"""
        ax4.plot(allele_frequency_result[0], label=r"$\pi_A$", linewidth=7)
        ax4.set_title("(d)", loc="left")
        ax4.set_xlabel("generation")
        ax4.set_ylabel("allele frequency")
        ax4.legend(shadow=True, prop={"size": 14})
        ax4.grid()
    else:
        plt.plot(allele_frequency_result[0], label="allele1")
        plt.plot(allele_frequency_result[1], label="allele2")
        plt.title("two steps; allele frequency of first allele of each step")
        plt.legend(shadow=True)
        plt.grid()
        # export_pdf.savefig()
        plt.close()


def make_graph_proportion_genotype(proportion_genotype_change):
    workdir = os.getcwd()
    path = workdir + "\\genotype_proportion.pdf"

    with PdfPages(path) as export_pdf:
        plt.plot(proportion_genotype_change[0], label="AaBb", linestyle="dotted")
        plt.plot(proportion_genotype_change[1], label="AaBB", linestyle="dashed")
        plt.plot(proportion_genotype_change[2], label="Aabb", linestyle="dashdot")
        plt.plot(proportion_genotype_change[3], label="AABb", linestyle=(0, (5, 10)))
        plt.plot(proportion_genotype_change[4], label="AABB", linestyle=(0, (5, 10)))
        plt.plot(proportion_genotype_change[5], label="AAbb", linestyle=(0, (3, 10, 1, 10)))
        plt.plot(proportion_genotype_change[6], label="aaBb", linestyle=(0, (3, 5, 1, 5)))
        plt.plot(proportion_genotype_change[7], label="aaBB", linestyle=(0, (3, 10, 1, 10, 1, 10)))
        plt.plot(proportion_genotype_change[8], label="aabb", linestyle=(0, (1, 1)))
        plt.legend(shadow=True)
        plt.title("genotype proportions in population")
        plt.grid()
        export_pdf.savefig()
        plt.close()


def make_concentration_graph_oneStep(t, ode_solution_collection, products_combined, axEnzyme, axProduct):
    # plot enzyme and complex concentrations
    for i in range(3):
        if i == 0:
            axEnzyme.plot(t, ode_solution_collection[i][0].T + ode_solution_collection[i][1].T,
                          label=r"$e_{AB}$", linestyle="solid", color="tab:green", linewidth=7)
            # axEnzyme.plot(t, ode_solution_collection[i][1].T, label="e1'.het", linestyle="solid")
            # note at index 2 would be substrate which is plotted elsewhere
            axEnzyme.plot(t, ode_solution_collection[i][3].T + ode_solution_collection[i][4].T,
                          label=r"$c_{AB}$", linestyle=(0, (1, 1)), color="tab:green", linewidth=7)
            # axEnzyme.plot(t, ode_solution_collection[i][4].T, label="c1'.het", linestyle="solid")
        elif i == 1:
            axEnzyme.plot(t, ode_solution_collection[i][0].T, label=r"$e_{AA}$", linestyle="solid",
                          color="tab:orange", linewidth=7)
            axEnzyme.plot(t, ode_solution_collection[i][3].T, label=r"$c_{AA}$", linestyle=(0, (1, 1)),
                          color="tab:orange", linewidth=7)
        else:
            axEnzyme.plot(t, ode_solution_collection[i][1].T, label=r"$e_{BB}$", linestyle="solid",
                          color="tab:blue", linewidth=7)
            axEnzyme.plot(t, ode_solution_collection[i][4].T, label=r"$c_{BB}$", linestyle=(0, (1, 1)),
                          color="tab:blue", linewidth=7)

    axEnzyme.legend(shadow=True, prop={"size": 14})
    axEnzyme.set_xlabel("time")
    axEnzyme.set_ylabel("concentration")
    axEnzyme.set_title("(a)", loc="left")
    axEnzyme.grid()

    # product and substrate concentrations
    max_product = [max(x) for x in products_combined]
    print("products: AB:", products_combined[0][-1],
          "AA", products_combined[1][-1],
          "BB", products_combined[2][-1])
    for i in range(3):
        if i == 0:
            axProduct.plot(t, ode_solution_collection[i][5] + ode_solution_collection[i][6],
                           label=r"$p_{AB}$", linestyle="solid", color="tab:green", linewidth=7)
            axProduct.plot(t, ode_solution_collection[i][2], label=r"$s_{AB}$", linestyle=(0, (1, 1)),
                           color="tab:green", linewidth=7)
        elif i == 1:
            axProduct.plot(t, ode_solution_collection[i][5], label=r"$p_{AA}$", linestyle="solid",
                           color="tab:orange", linewidth=7)
            axProduct.plot(t, ode_solution_collection[i][2], label=r"$s_{AA}$", linestyle=(0, (1, 1)),
                           color="tab:orange", linewidth=7)
        else:
            axProduct.plot(t, ode_solution_collection[i][6], label=r"$p_{BB}$", linestyle="solid",
                           color="tab:blue", linewidth=7)
            axProduct.plot(t, ode_solution_collection[i][2], label=r"$s_{BB}$", linestyle=(0, (1, 1)),
                           color="tab:blue", linewidth=7)

    axProduct.set_ylim(0, max(max_product) * 1.05)
    axProduct.legend(shadow=True, prop={"size": 14})
    axProduct.set_xlabel("time")
    axProduct.set_title("(b)", loc="left")
    axProduct.grid()


def run_ODE(step_count, time_span, init, enzyme_kinetics_list, t, discrete_generations):
    if step_count == 1:
        # iterate over genotypes
        products_1 = []
        ode_solution_collection = []

        workdir = os.getcwd()
        substrate_flux = str(enzyme_kinetics_list[-1][0])
        substrate_flux = substrate_flux.replace(".", "")
        title_append = ""
        if enzyme_kinetics_list[-1][1]:
            title_append = "_withPerturbations"
        path = workdir + "\\concentrations_substrateFlux-" + substrate_flux + title_append + ".pdf"
        with PdfPages(path) as export_pdf:
            # fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, sharex=True)
            plt.rcParams["figure.figsize"] = (16, 9)
            plt.rc('font', size=22)
            fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)  # figsize=(16, 9)
            for i in range(3):
                sol = solve_ivp(ODE_system_oneStep, t_span=[0, time_span], y0=init[i], args=[enzyme_kinetics_list],
                                dense_output=True)
                ode_solution = sol.sol(t)
                ode_solution_collection.append(ode_solution)

                """if i == 0:
                    make_graph_oneStep(ode_solution, t, i, ax1, ax2)
                elif i == 1:
                    make_graph_oneStep(ode_solution, t, i, ax3, ax4)
                else:
                    make_graph_oneStep(ode_solution, t, i, ax5, ax6)"""

                # calculate dominance
                product_combined_1 = [sum(x) for x in zip(ode_solution[5], ode_solution[6])]
                products_1.append(product_combined_1)
            # plt.subplots_adjust(hspace=0.5)
            # plt.suptitle("enzyme, complex and product concentrations")
            # export_pdf.savefig()
            # plt.close()
            make_concentration_graph_oneStep(t, ode_solution_collection, products_1, ax1, ax2)
            # export_pdf.savefig(bbox_inches="tight")
            # plt.close()

            # fig, (ax1, ax2) = plt.subplots(2)
            # ax1.set_title("dominance and allele frequency of first enzyme")
            dominance_result = calc_dominance_oneStep(products_1)
            make_dominance_graph_oneStep(t, dominance_result, ax3)

            # TODO implement allele frequency calculations
            allele_frequencies = allele_frequency_calc(step_count, discrete_generations, [products_1],
                                                       enzyme_kinetics_list[-1][1])
            make_graph_alleleFrequency(step_count, allele_frequencies, ax4)
            plt.subplots_adjust(hspace=0.5)
            export_pdf.savefig(bbox_inches="tight")
            plt.close()

    elif step_count == 2:
        # 3^1 genotypes for one step, 3^2 for two steps
        products_1 = []
        products_2 = []
        for i in range(9):
            sol = solve_ivp(ODE_system_twoStep, t_span=[0, time_span], y0=init[i], args=[enzyme_kinetics_list],
                            dense_output=True)
            ode_solution = sol.sol(t)
            make_graph_twoStep(ode_solution, t, i)

            product_combined_1 = [sum(x) for x in zip(ode_solution[10], ode_solution[11])]
            product_combined_2 = [sum(x) for x in zip(ode_solution[12], ode_solution[13])]
            products_1.append(product_combined_1)
            products_2.append(product_combined_2)

        dominance_result_twoStep = calc_dominance_twoStep(products_1, products_2)
        make_dominance_graph_twoStep(t, dominance_result_twoStep)

        # TODO implement allele frequency calculations
        allele_frequencies = allele_frequency_calc(step_count, discrete_generations, [products_2])
        make_graph_alleleFrequency(step_count, allele_frequencies)


def run_ODE_parameterComparison(step_count, time_span, init, enzyme_kinetics_list, t,
                                discrete_generations, max_factor, mutation_mode,
                                unmutated_parameter, perturbation_mode):
    result_list = []
    enzyme_kinetics_list_old = copy.deepcopy(enzyme_kinetics_list)

    if step_count == 1:
        parameters_to_change = ["kr1", "kcat1"]
        if mutation_mode:
            fixed_parameter = enzyme_kinetics_list_old[-1][2]
            parameters_to_change = [parameters_to_change[fixed_parameter]]
        else:
            if unmutated_parameter == 3:
                # kf is mutated
                enzyme_kinetics_list_old[0][0] *= 50  # second dimension array relates to kinetics parameter (0 -> kf, 1 -> kr, 3 -> kcat)
                parameters_to_change = ["kr1", "kcat1"]  # x axis i.e., change in other enzymes
            else:
                parameters_to_change = ["kf1", "kcat1"]
        parameter_index = -1

        # iterate over genotypes
        for parameter in parameters_to_change:
            result_list.append([])
            parameter_index += 1
            for i in range(max_factor):
                if mutation_mode:
                    enzyme_kinetics_list_new = mutation_mode_enzyme_kinetics_list(enzyme_kinetics_list_old, (i + 1),
                                                                                  unmutated_parameter)
                else:
                    enzyme_kinetics_list_new = change_enzyme_kinetics_oneStep(enzyme_kinetics_list_old, parameter,
                                                                              (i + 1), unmutated_parameter)
                print(parameter, (i + 1), "kinetics ->", enzyme_kinetics_list_new)
                products_1 = []
                for j in range(3):
                    sol = solve_ivp(ODE_system_oneStep, t_span=[0, time_span], y0=init[j],
                                    args=[enzyme_kinetics_list_new], dense_output=True)
                    ode_solution = sol.sol(t)

                    # calculate dominance
                    product_combined_1 = [sum(x) for x in zip(ode_solution[5], ode_solution[6])]
                    products_1.append(product_combined_1)

                    if i == 0:
                        result_list[parameter_index].append([product_combined_1[-1]])
                    else:
                        result_list[parameter_index][j].append(product_combined_1[-1])

                dominance_result = calc_dominance_oneStep(products_1)
                if i == 0:
                    result_list[parameter_index].append([dominance_result[0][-1]])
                else:
                    result_list[parameter_index][3].append(dominance_result[0][-1])

                allele_frequencies = allele_frequency_calc(step_count, discrete_generations, [products_1], perturbation_mode)
                factor = int(3 / 4 * len(allele_frequencies[0]))
                if i == 0:
                    result_list[parameter_index].append([max(allele_frequencies[0][factor:])])
                else:
                    result_list[parameter_index][-1].append(max(allele_frequencies[0][factor:]))
    elif step_count == 2:
        # read: [sflux, kf1, kr1, kcat1, kf2, kr2, kcat2, kf3, kr3, kcat3, kf4, kr4, kcat4]
        result_list = []
        """parameters_to_change = ["sflux", "kf1", "kr1", "kcat1",
                                "kf2", "kr2", "kcat2",
                                "kf3", "kr3", "kcat3",
                                "kf4", "kr4", "kcat4"]"""
        parameters_to_change = ["sflux", "kf2", "kr1", "kcat2", "kf4", "kr3", "kcat4"]
        parameter_index = -1
        for parameter in parameters_to_change:
            result_list.append([])
            parameter_index += 1
            step_flux = []
            for i in range(max_factor):
                enzyme_kinetics_list_new = change_enzyme_kinetics(enzyme_kinetics_list_old, parameter, (i + 1))
                print(parameter, i + 1, ": kinetics ->", enzyme_kinetics_list_new)
                products_1 = []
                products_2 = []
                for j in range(9):
                    sol = solve_ivp(ODE_system_twoStep, t_span=[0, time_span], y0=init[j],
                                    args=[enzyme_kinetics_list_new], dense_output=True)
                    ode_solution = sol.sol(t)

                    product_combined_1 = [sum(x) for x in zip(ode_solution[10], ode_solution[11])]
                    product_combined_2 = [sum(x) for x in zip(ode_solution[12], ode_solution[13])]
                    products_1.append(product_combined_1)
                    products_2.append(product_combined_2)

                    # todo step product flux
                    step1_flux = product_combined_1[-1] - product_combined_1[-2]
                    step2_flux = product_combined_2[-1] - product_combined_2[-2]
                    if i == 0:
                        step_flux.append([[step1_flux], [step2_flux]])
                    else:
                        step_flux[j][0].append(step1_flux)
                        step_flux[j][1].append(step2_flux)

                    # append product change
                    if i == 0:
                        result_list[parameter_index].append([[product_combined_1[-1]], [product_combined_2[-1]]])
                    else:
                        count = 0
                        for elem in result_list[parameter_index][j]:
                            if count == 0:
                                elem.append(product_combined_1[-1])
                                count += 1
                            else:
                                elem.append(product_combined_2[-1])
                # calculation of dominance
                dominance_result_twoStep = calc_dominance_twoStep(products_1, products_2)
                dominance_1 = dominance_result_twoStep[0]
                dominance_2 = dominance_result_twoStep[1]
                if i == 0:
                    for k in range(3):
                        dominance_iter = dominance_1[k][0]
                        result_list[parameter_index].append([dominance_iter[-1]])
                    for k in range(3):
                        dominance_iter = dominance_2[k][0]
                        result_list[parameter_index].append([dominance_iter[-1]])
                    for k in range(3):
                        # dominance step 1 related to fitness
                        dominance_iter = dominance_1[k][1]
                        result_list[parameter_index].append([dominance_iter[-1]])
                else:
                    for k in range(3):
                        dominance_iter = dominance_1[k][0]
                        result_list[parameter_index][9 + k].append(dominance_iter[-1])
                    for k in range(3):
                        dominance_iter = dominance_2[k][0]
                        result_list[parameter_index][12 + k].append(dominance_iter[-1])
                    for k in range(3):
                        # dominance step 1 related to fitness
                        dominance_iter = dominance_1[k][1]
                        result_list[parameter_index][15 + k].append(dominance_iter[-1])

                allele_frequencies = allele_frequency_calc(step_count, discrete_generations, [products_2])
                factor = int(3 / 4 * len(allele_frequencies[0]))
                max_frequency_1 = max(allele_frequencies[0][factor:])
                max_frequency_2 = max(allele_frequencies[1][factor:])
                if i == 0:
                    result_list[parameter_index].append([max_frequency_1])
                    result_list[parameter_index].append([max_frequency_2])
                else:
                    result_list[parameter_index][-2].append(max_frequency_1)
                    result_list[parameter_index][-1].append(max_frequency_2)
            result_list[parameter_index].append(step_flux)
    result_list.append(parameters_to_change)
    count = 0
    for elem in result_list:
        print(count, elem)
        count += 1
    return result_list


def change_enzyme_kinetics_oneStep(enzyme_kinetics_list_old, parameter, factor, comparison_mode):
    enzyme_kinetics_list_new = copy.deepcopy(enzyme_kinetics_list_old)  # note -- maybe redundant

    if parameter == "sflux":
        enzyme_kinetics_list_new[2][0] = round(enzyme_kinetics_list_new[2][0] * factor, 10)
    elif parameter == "kf1":
        if comparison_mode == 3:
            enzyme_kinetics_list_new[0][0] = round(enzyme_kinetics_list_new[0][0] * factor, 10)
            enzyme_kinetics_list_new[1][0] = round(enzyme_kinetics_list_new[1][0] * factor, 10)
        else:
            enzyme_kinetics_list_new[0][0] = round(enzyme_kinetics_list_new[0][0] * factor, 10)
    elif parameter == "kr1":
        if comparison_mode == 3:
            enzyme_kinetics_list_new[0][1] = round(enzyme_kinetics_list_new[0][1] * factor, 10)
            enzyme_kinetics_list_new[1][1] = round(enzyme_kinetics_list_new[1][1] * factor, 10)
        else:
            enzyme_kinetics_list_new[0][1] = round(enzyme_kinetics_list_new[0][1] * factor, 10)
    elif parameter == "kcat1":
        if comparison_mode == 3:
            enzyme_kinetics_list_new[0][2] = round(enzyme_kinetics_list_new[0][2] * factor, 10)
            enzyme_kinetics_list_new[1][2] = round(enzyme_kinetics_list_new[1][2] * factor, 10)
        else:
            enzyme_kinetics_list_new[0][2] = round(enzyme_kinetics_list_new[0][2] * factor, 10)
    elif parameter == "kf2":
        enzyme_kinetics_list_new[1][0] = round(enzyme_kinetics_list_new[1][0] * factor, 10)
    elif parameter == "kr2":
        enzyme_kinetics_list_new[1][1] = round(enzyme_kinetics_list_new[1][1] * factor, 10)
    else:
        enzyme_kinetics_list_new[1][2] = round(enzyme_kinetics_list_new[1][2] * factor, 10)

    return enzyme_kinetics_list_new


def change_enzyme_kinetics(enzyme_kinetics_list_old, parameter, factor):
    enzyme_kinetics_list_new = copy.deepcopy(enzyme_kinetics_list_old)

    if parameter == "sflux":
        enzyme_kinetics_list_new[4][0] = round(enzyme_kinetics_list_new[4][0] * factor, 10)
    elif parameter == "kf1":
        enzyme_kinetics_list_new[0][0] = round(enzyme_kinetics_list_new[0][0] * factor, 10)
    elif parameter == "kr1":
        enzyme_kinetics_list_new[0][1] = round(enzyme_kinetics_list_new[0][1] * factor, 10)
    elif parameter == "kcat1":
        enzyme_kinetics_list_new[0][2] = round(enzyme_kinetics_list_new[0][2] * factor, 10)
    elif parameter == "kf2":
        enzyme_kinetics_list_new[1][0] = round(enzyme_kinetics_list_new[1][0] * factor, 10)
    elif parameter == "kr2":
        enzyme_kinetics_list_new[1][1] = round(enzyme_kinetics_list_new[1][1] * factor, 10)
    elif parameter == "kcat2":
        enzyme_kinetics_list_new[1][2] = round(enzyme_kinetics_list_new[1][2] * factor, 10)
    elif parameter == "kf3":
        enzyme_kinetics_list_new[2][0] = round(enzyme_kinetics_list_new[2][0] * factor, 10)
    elif parameter == "kr3":
        enzyme_kinetics_list_new[2][1] = round(enzyme_kinetics_list_new[2][1] * factor, 10)
    elif parameter == "kcat3":
        enzyme_kinetics_list_new[2][2] = round(enzyme_kinetics_list_new[2][2] * factor, 10)
    elif parameter == "kf4":
        enzyme_kinetics_list_new[3][0] = round(enzyme_kinetics_list_new[3][0] * factor, 10)
    elif parameter == "kr4":
        enzyme_kinetics_list_new[3][1] = round(enzyme_kinetics_list_new[3][1] * factor, 10)
    else:
        enzyme_kinetics_list_new[3][2] = round(enzyme_kinetics_list_new[3][2] * factor, 10)

    return enzyme_kinetics_list_new


def make_graph_parameterComparison(comparison_result, max_factor, substrate_flux, step_count, enzyme_concentrations,
                                   comparison_mode):
    workdir = os.getcwd()
    flux_title = str(substrate_flux).replace(".", "")
    path = (workdir + "\\parameter-comparison_enzymeConcentrations-" + str(enzyme_concentrations[0]) + "-" +
            str(enzyme_concentrations[1]) + "_substrateFlux-" + flux_title + ".pdf")

    count = 0
    parameters_to_change = comparison_result[-1]
    del comparison_result[-1]
    with PdfPages(path) as export_pdf:
        for parameter in comparison_result:
            title_append = ""
            x_axis = []
            # todo calcluate michaelis-menten constant for each rate constant change
            kM_value = []
            title_append = parameters_to_change[count]
            if title_append == "sflux":
                for i in range(max_factor):
                    x_axis.append(round(substrate_flux * (i + 1), 10))
            elif title_append[:2] == "kf":
                # print("test1")
                for i in range(max_factor):
                    x_axis.append(round(0.001 * (i + 1), 10))
                    kM_value.append(round((2 * 0.001) / x_axis[i], 10))
            else:
                # print("test2")
                for i in range(max_factor):
                    x_axis.append(round(0.001 * (i + 1), 10))
                    # kM_value.append(round((x_axis[i] + 0.001) / 0.01, 10))
            if step_count == 2:
                genotypes = ["AaBb", "AaBB", "Aabb", "AABb", "AABB", "AAbb", "aaBb", "aaBB", "aabb"]
                for i in range(step_count):
                    ax = plt.axes(projection="3d")
                    y_projection = [0] * max_factor
                    for genotype in range(3 ** step_count):
                        for k in range(max_factor):
                            y_projection[k] = genotype
                        ax.plot3D(x_axis, y_projection, parameter[genotype][i], label=genotypes[genotype])
                    plt.legend(shadow=True, title="genotype", loc="upper left")
                    ax.set_title("products for " + title_append + " increase at step " + str(i + 1))
                    ax.set_xlabel(title_append)
                    ax.set_zlabel("product concentration")
                    plt.tight_layout()
                    export_pdf.savefig(bbox_inches="tight")
                    plt.close()

                    step_flux = parameter[-1]
                    ax = plt.axes(projection="3d")
                    y_projection = [0] * max_factor
                    for genotype in range(3 ** step_count):
                        for k in range(max_factor):
                            y_projection[k] = genotype
                        ax.plot3D(x_axis, y_projection, step_flux[genotype][i], label=genotypes[genotype])
                    plt.legend(shadow=True, title="genotypes", loc="upper left")
                    ax.set_title("step " + str(i + 1) + " output flux")
                    ax.set_xlabel(title_append)
                    ax.set_zlabel("product change")
                    plt.tight_layout()
                    export_pdf.savefig(bbox_inches="tight")
                    plt.close()

                    other_step_genotype = ["het", "hom1", "hom2"]
                    for j in range(3):
                        plt.plot(x_axis, parameter[j + 9 + (3 * i)],
                                 label="genotype other step " + other_step_genotype[j])
                        plt.title("dominance first enzyme of step " + str(i + 1) + " for " + title_append)
                    plt.legend(shadow=True)
                    plt.grid()
                    export_pdf.savefig(bbox_inches="tight")
                    plt.close()

                    if i == 0:
                        for j in range(3):
                            plt.plot(x_axis, parameter[j + 15], label="genotype other step " + other_step_genotype[j])
                            plt.title("dominance enzyme 1 related to fitness (products other step)")
                        plt.legend(shadow=True)
                        plt.grid()
                        export_pdf.savefig(bbox_inches="tight")
                        plt.close()

                plt.plot(x_axis, parameter[-3], label="allele 1")
                plt.plot(x_axis, parameter[-2], label="allele 2")
                plt.title("allele frequencies for " + title_append)
                plt.legend(shadow=True)
                plt.grid()
                export_pdf.savefig(bbox_inches="tight")
                plt.close()
            else:
                if comparison_mode == 3:
                    if title_append == "kf1":
                        title_append = r"$k_{f,A}$=$k_{f,B}$"
                    elif title_append == "kr1":
                        title_append = r"$k_{r,A}$=$k_{r,B}$"
                    elif title_append == "kcat1":
                        title_append = r"$k_{cat,A}$=$k_{cat,B}$"
                    elif title_append == "sflux":
                        title_append = r"$s_{flux}$"
                # plt.rcParams["figure.figsize"] = (6, 6)
                plt.rc('font', size=22)
                fig, (ax1, ax2, ax3) = plt.subplots(3, figsize=(20/2.54, 55/2.54))  # division by 2.54 converts cm to inch
                # print(title_append, kM_value)
                genotypes = ["AB", "AA", "BB"]

                # plot products
                dominance_indicator = []
                print("ex.:", float(parameter[1][0]))
                for genotype in range(3):
                    # plt.plot(x_axis, parameter[genotype], label=genotypes[genotype])
                    ax1.plot(x_axis, parameter[genotype], label=genotypes[genotype], linewidth=7)
                if count == 0 or count == 1 or count == 2 or count == 3:
                    for i in range(len(parameter[0])):
                        # todo --- plot sqrt(AA,BB)
                        dominance_indicator.append(np.sqrt(float(parameter[1][i]) * float(parameter[2][i])))
                    ax1.plot(x_axis, dominance_indicator, label="sqrt(AA*BB)", linewidth=7, linestyle=(0, (1, 1)))
                    # todo --- vertical line for (approximate) intersection het-sqrt
                    het_sqrt_intersections = find_het_sqrtAABB_intersection(dominance_indicator, parameter[0])
                for intersection in het_sqrt_intersections:
                    ax1.axvline(x=0.001*intersection, color="grey")  # to match x axis
                    print(het_sqrt_intersections)
                ax1.legend(shadow=True)
                if count == 0:
                    ax1.set_title("(a)", loc="left")
                elif count == 1:
                    ax1.set_title("(b)", loc="left")
                elif count == 2:
                    ax1.set_title("(g)", loc="left")
                elif count == 3:
                    ax1.set_title("(h)", loc="left")
                ax1.set_xlabel(title_append)
                ax1.set_ylabel("product concentration")
                ax1.grid()

                # plot dominance of first enzyme
                ax2.plot(x_axis, parameter[3], label="A", linewidth=7)
                for intersection in het_sqrt_intersections:
                    ax2.axvline(x=0.001*intersection, color="grey")  # to match x axis
                ax2.legend(shadow=True)
                if count == 0:
                    ax2.set_title("(c)", loc="left")
                elif count == 1:
                    ax2.set_title("(d)", loc="left")
                elif count == 2:
                    ax2.set_title("(i)", loc="left")
                elif count == 3:
                    ax2.set_title("(j)", loc="left")
                ax2.set_ylabel("dominance")
                ax2.set_xlabel(title_append)
                ax2.grid()

                # plot allele frequency
                ax3.plot(x_axis, parameter[-1], label="A", linewidth=7)
                for intersection in het_sqrt_intersections:
                    ax3.axvline(x=0.001*intersection, color="grey")  # to match x axis
                if count == 0:
                    ax3.set_title("(e)", loc="left")
                elif count == 1:
                    ax3.set_title("(f)", loc="left")
                elif count == 2:
                    ax3.set_title("(k)", loc="left")
                elif count == 3:
                    ax3.set_title("(l)", loc="left")
                ax3.legend(shadow=True)
                ax3.set_xlabel(title_append)
                ax3.set_ylabel("maximum allele frequency")
                ax3.grid()

                plt.subplots_adjust(hspace=0.5)
                export_pdf.savefig(bbox_inches="tight")
                plt.close()
            count += 1


def find_het_sqrtAABB_intersection(dominance_indicator, product_heterozygous):
    result = []
    for i in range(len(dominance_indicator) - 1):
        difference1 = dominance_indicator[i] - product_heterozygous[i]
        difference2 = dominance_indicator[i + 1] - product_heterozygous[i + 1]
        if np.sign(difference1) != np.sign(difference2):
            print("before intersection: dominance indicator", dominance_indicator[i],
                  "and het products", product_heterozygous[i],
                  "\nafter intersection: dominance indicator", dominance_indicator[i + 1],
                  "and het products", product_heterozygous[i + 1])
            result.append((2 * i + 1)/2)  # approximate intersection

    return result

def fixed_mutation_different_kinetics(max_mutation_factor, step_count, time_span, init, enzyme_kinetics_list, t,
                                      discrete_generations, max_factor, mutation_mode, perturbation_mode):
    parameters = [r"$kf_1$", r"$kr_1$", r"$kcat_1$"]
    workdir = os.getcwd()
    path = (workdir + "\\fixed_mutation.pdf")
    with PdfPages(path) as export_pdf:
        for i in range(3):
            fixed_parameter = parameters[i]
            parameter_base_value = enzyme_kinetics_list[0][i]
            result_list = []
            enzyme_kinetics_list[-1][2] = i

            for j in range(3):
                if i != j:
                    for mutation_factor in range(max_mutation_factor):
                        enzyme_kinetics_list[0][i] = round(parameter_base_value * (mutation_factor + 1), 10)
                        comparison_result = run_ODE_parameterComparison(step_count, time_span, init, enzyme_kinetics_list,
                                                                        t, discrete_generations, max_factor, mutation_mode,
                                                                        j, perturbation_mode)
                        result_list.append(comparison_result)
                    # print("test --", result_list[0])
                    fig = plt.figure()
                    make_graph_mutation_mode(result_list, fixed_parameter, max_mutation_factor, max_factor,
                                             fig, export_pdf, j)
                    result_list = []
                    enzyme_kinetics_list[0][i] = parameter_base_value


def make_graph_mutation_mode(mutation_mode_result, fixed_parameter, max_mutation_factor, max_factor, fig,
                             export_pdf, unmutated_parameter):
    x = [x + 1 for x in range(max_mutation_factor)]
    y = [x + 1 for x in range(max_factor)]
    X, Y = np.meshgrid(x, y)

    x2 = [x for x in range(1, max_mutation_factor + 1)]
    y2 = [x for x in range(1, max_factor + 1)]
    z2 = [0.5 for x in range(max_factor * max_mutation_factor)]
    X2, Y2 = np.meshgrid(x2, y2)
    Z2 = np.reshape(z2, X2.shape)

    for i in range(5):
        z = []
        for j in range(max_mutation_factor):
            parameter_comparison_result = mutation_mode_result[j][0]
            # print("test --", parameter_comparison_result)
            z.append(parameter_comparison_result[i])
            # print("test1 --", parameter_comparison_result[0][3])

        # transpose because column names should refer to y axis
        df = DataFrame(z, columns=[(x + 1) for x in range(max_factor)]).T
        # print(df)

        ax = plt.axes(projection="3d")
        """if i == 3 or i == 4:
            # plot 0.5 surface
            ax.plot_surface(X2, Y2, Z2, cmap="inferno", alpha=0.5)"""
        # ax.plot_surface(X, Y, df, cmap="winter", edgecolor="none")
        ax.plot_wireframe(X, Y, df)
        if i == 0:
            ax.set_title("products -- Aa")
        elif i == 1:
            ax.set_title("products -- AA")
        elif i == 2:
            ax.set_title("products -- aa")
        elif i == 3:
            ax.set_title("dominance -- A")
        else:
            ax.set_title("maximum allele frequency -- A")
        ax.set_xlabel(fixed_parameter + r"$\times$ factor")
        if unmutated_parameter == 0:
            ax.set_ylabel(r"($kf_1$, $kf_2$) $\times$ factor")
        elif unmutated_parameter == 1:
            ax.set_ylabel(r"($kr_1$, $kr_2$) $\times$ factor")
        else:
            ax.set_ylabel(r"($kcat_1$, $kcat_2$) $\times$ factor")
        if i < 3:
            ax.set_zlabel("product")
        elif i == 3:
            ax.set_zlabel("dominance")
        else:
            ax.set_zlabel("maximum allele frequency")
        # plt.show()
        ax.view_init(45)
        export_pdf.savefig()
        plt.close()


def mutation_mode_enzyme_kinetics_list(enzyme_kinetics_list_old, factor, unmutated_parameter):
    enzyme_kinetics_list_new = copy.deepcopy(enzyme_kinetics_list_old)
    fixed_parameter = enzyme_kinetics_list_new[-1][2]

    for i in range(2):
        for j in range(3):
            if j == unmutated_parameter:
                enzyme_kinetics_list_new[i][j] = round(enzyme_kinetics_list_new[i][j] * factor, 10)

    return enzyme_kinetics_list_new


def main():
    starting_operational_params = [line.strip() for line in open("starting_operational_parameters.txt", "r")]
    print("starting operational parameters: " + str(starting_operational_params))

    # extract information and organize in named variables
    step_count = int(starting_operational_params[0])  # either 1 or 2
    time_span = float(starting_operational_params[1])  # time span for ODE system
    substrate_flux = float(starting_operational_params[3])  # substrate flux for ODE
    perturbation_mode = bool(int(starting_operational_params[4]))
    discrete_generations = int(starting_operational_params[9])  # discrete generations for allele frequency calculations
    parameter_comparison_mode = int(starting_operational_params[10])  # 0: no, 1: yes, 3: other
    max_factor = int(starting_operational_params[11])  # maximum parameter increase factor
    mutation_mode = bool(int(starting_operational_params[12]))  # yes/no use fixed mutation different kinetics mode

    # check step count
    if not ((step_count == 1) or (step_count == 2)):
        return print("wrong step count (this program supports either 1 or 2 steps)")

    # parameters for parameter_comparison and mutation_mode
    init = initial_state(starting_operational_params)
    enzyme_kinetics_list = enzyme_kinetics(step_count)
    print(enzyme_kinetics_list)
    enzyme_kinetics_list.append([substrate_flux, perturbation_mode, 0])  # the 0 is for mutation mode
    t = np.linspace(0, time_span)

    if mutation_mode == 1:
        max_mutation_factor = int(starting_operational_params[13])
        fixed_mutation_different_kinetics(max_mutation_factor, step_count, time_span, init, enzyme_kinetics_list, t,
                                          discrete_generations, max_factor, mutation_mode, perturbation_mode)
    elif parameter_comparison_mode == 1 or parameter_comparison_mode == 3:
        comparison_result = run_ODE_parameterComparison(step_count, time_span, init, enzyme_kinetics_list, t,
                                                        discrete_generations, max_factor, mutation_mode,
                                                        parameter_comparison_mode, perturbation_mode)

        enzyme_concentrations = [int(starting_operational_params[5]), int(starting_operational_params[6])]
        make_graph_parameterComparison(comparison_result, max_factor, substrate_flux, step_count, enzyme_concentrations,
                                       parameter_comparison_mode)
    else:
        # create enzyme kinetics/rate constants (kr, kf, kcat) information for each enzyme
        enzyme_kinetics_list = enzyme_kinetics(step_count)
        enzyme_kinetics_list.append([substrate_flux, perturbation_mode])
        print(enzyme_kinetics_list)

        # calculate initial states for ODE system
        init = initial_state(starting_operational_params)
        count = 0
        for state in init:
            print(str(count) + ": " + str(state))
            count += 1

        t = np.linspace(0, time_span)
        # run ODE
        run_ODE(step_count, time_span, init, enzyme_kinetics_list, t, discrete_generations)


# start main program
if __name__ == "__main__":
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
